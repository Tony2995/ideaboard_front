import React, { Component } from 'react'

const Idea = ({idea}) =>
    <div className="title" key={idea.id}>
        <h2>{idea.title}</h2>
        <p>{idea.body}</p>
    </div>

export default Idea