import React, {Component} from 'react'
import axios from 'axios'
import '../App.css'
import Idea from './Idea'

class IdeasContainer extends Component {

    constructor(props) {
        super(props);
        this.state = {
            ideas: []
        }
    }

    componentDidMount() {
        axios.get('http://localhost:3001/api/v1/ideas')
            .then(response => {
                this.setState({ideas: response.data})
            })
            .catch(error => console.log(error))
    }

    render() {
        return (
            <div>
                {this.state.ideas.map((idea) => {
                    return (<Idea idea={idea} key={idea.id} />)
                })}
            </div>
        )
    }


}

export default IdeasContainer;